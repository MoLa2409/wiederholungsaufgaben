package com.mycompany.wiederholung;

import javax.swing.JOptionPane;

public class Aufgabe1 {
    
    public static void main(String[] args){
         double pi = Math.PI;
         double durch = Integer.parseInt(JOptionPane.showInputDialog("Geben Sie den Durchesser ein:"));
         double ergUm;
         double ergFl;
         
         ergUm = 2*pi*(durch/2);
         ergFl = durch/2 * (ergUm/2);
         
         System.out.println("Die Fläche beträgt "+ergFl+" und der Umfang beträgt "+ergUm);
    }
    
}
