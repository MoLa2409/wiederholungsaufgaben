package com.mycompany.wiederholung;

import javax.swing.JOptionPane;

public class Aufgabe2 {

    public static void main(String[] args) {
        int Schüler = Integer.parseInt(JOptionPane.showInputDialog("Wie viele Schüler?"));
        int Begleitpersonen = Integer.parseInt(JOptionPane.showInputDialog("Wie viele Begleitpersonen?"));
        double km = Double.parseDouble(JOptionPane.showInputDialog("Wie viele km?"));
        
        double fahrpreis = km * 0.05;
        double fahrpreisGes = (km * 0.05)*Schüler;
        double fahrpreisBegl = fahrpreis/2;
        double fahrpreisBeglGes = fahrpreisBegl * Begleitpersonen;
        double freifahrtenpreis = 0;
        
        if(Schüler>10){
            int freifahrten = Schüler / 10;
            freifahrtenpreis = freifahrten * fahrpreis;
        }
        
        
        double gesamtpreis = fahrpreisGes+fahrpreisBeglGes-freifahrtenpreis;
        double einzelpreis = fahrpreis;
        
        System.out.println("Der Gesamtpreis beträgt "+gesamtpreis);
        System.out.println("Der Einzelpreis für Schuüler beträgt "+einzelpreis);
        System.out.println("Der Einzelpreis für Begleitpersonen beträgt "+fahrpreisBegl);
    }

}
