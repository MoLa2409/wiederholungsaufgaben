package com.mycompany.wiederholung;

import java.util.Random;

public class Aufgabe4 {

    public static void main(String[] args) {
        Random x = new Random();
        int Kopf = 0;
        int Zahl = 0;
        int anzahlK = 0;
        int anzahlZ = 0;
        int streak = 0;

        for (int i = 0; i < 100; i++) {
            int n = x.nextInt(2);

            if (n == 0) {
                anzahlK++;
                Kopf = Kopf + 1;
                if (anzahlK > streak) {
                    streak = anzahlK;
                }
                anzahlZ = 0;
            } else {
                anzahlZ++;
                Zahl = Zahl + 1;
                if (anzahlZ > streak) {
                    streak = anzahlZ;
                }
                anzahlK = 0;
            }
        }

        System.out.println("Anzahl von Kopf: " + Kopf);
        System.out.println("Anzahl von Zahl: " + Zahl);
        System.out.println("Die höchste Streak ist: " + streak);
    }
}