package com.mycompany.wiederholung;

import java.util.Random;
import javax.swing.JOptionPane;


public class Aufgabe5 {
    
    public static void main(String[] args) {
        Random x = new Random();
        int[] arr = new int[1000];
        int zahl = Integer.parseInt(JOptionPane.showInputDialog("Geben Sie die Zahl ein:"));
        int anzahl = 0;
        int position = 0;
        
        for (int i = 0; i < arr.length; i++) {
            int n = x.nextInt(101);
            arr[i] = n;
        }
        
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] == zahl){
                anzahl = anzahl + 1;
                position = i;
                System.out.println("Position: "+position);
            }
        } 
        System.out.println("Die Zahl kommt "+anzahl+" Mal vor");
    }
}
