package com.mycompany.wiederholung;

public class Rekursion {

    public static void main(String[] args) {
        System.out.println("10 + 243 ist " + addition(10, 243));
        System.out.println("13 * 13 ist " + multiplikation(13, 13));
        System.out.println(fFunktion(5));
        System.out.println("Ist 11 eine Primzahl? " + istPrimzahl(11, 10));
        System.out.println("Die Quersumme von 4392 ist " + Quersumme(4392));

        String palindrom = "lagerregal";
        char[] ch = palindrom.toCharArray();
        System.out.println("Ist lagerregal ein Palindrom? " + istPalindrom(ch, 0, ch.length - 1));

        System.out.println("Der Gröste gemeinsame Teiler von 160 und 256 ist " + ggT(160, 256));

    }

    public static int addition(int a, int b) {
        if (b <= 1) {
            return a + 1;
        } else {
            return addition(a + 1, b - 1);
        }
    }

    public static int multiplikation(int x, int y) {
        if (x == 0 || y == 0) {
            return 0;
        } else {
            return x + multiplikation(x, y - 1);
        }
    }

    public static int fFunktion(int a) {
        if (a == 1) {
            return 1;
        } else {
            return fFunktion(a - 1) + 2 * a - 1;
        }
    }

    public static boolean istPrimzahl(int n, int m) {
        if (m == 1) {
            return true;
        } else if (n % m == 0) {
            return false;
        } else {
            return istPrimzahl(n, m - 1);
        }
    }

    public static int Quersumme(int zahl) {
        if (zahl <= 9) {
            return zahl;
        } else {
            return zahl % 10 + Quersumme(zahl / 10);
        }
    }

    public static boolean istPalindrom(char[] wort, int start, int ende) {
        if (start >= ende) {
            return true;
        } else {
            return wort[start] == wort[ende]
                    && istPalindrom(wort, start + 1, ende - 1);

        }
    }

    public static int ggT(int x, int y) {
        if (x == y) {
            return x;
        } else if (x < y) {
            return ggT(x, y - x);
        } else {
            return ggT(x - y, y);
        }
    }
}
