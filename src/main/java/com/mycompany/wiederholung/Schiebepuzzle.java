package com.mycompany.wiederholung;

import java.util.Random;
import javax.swing.JOptionPane;

public class Schiebepuzzle {

    static int[][] spielfeld = new int[4][4];
    static int[][] fertigesSpiel = new int[4][4];
    static boolean überprüfen = false;

    public static void main(String[] args) {
        int benötigteZüge = 0;
        int Rekord = 0;
        int k = 1;

        positionen();

        for (int i = 0; i < fertigesSpiel.length; i++) {
            for (int j = 0; j < fertigesSpiel.length; j++) {
                if (k <= 15) {
                    fertigesSpiel[i][j] = k;
                    k++;
                } else {
                    fertigesSpiel[i][j] = 0;
                }

            }

        }

        while (überprüfen == false) {
            printSpielfeld();
            spielen();
            Überprüfen();
            System.out.println();
            benötigteZüge++;
        }
        printSpielfeld();
        System.out.println();
        System.out.println("Du hast es geschafft!");
        System.out.println("Benötigte Züge: " + benötigteZüge);
        
        if(benötigteZüge>Rekord){
            System.out.println("Sie haben einen neuen Rekord aufgestellt!");
            Rekord = benötigteZüge;
        }
        
    }

    public static void positionen() {
        Random r = new Random();
        for (int i = 0; i < 16; i++) {
            int f = r.nextInt(4);
            int f2 = r.nextInt(4);

            if (spielfeld[f][f2] == 0) {
                spielfeld[f][f2] = i;
            } else {
                i--;
            }
        }

    }

    public static void printSpielfeld() {
        for (int i = 0; i < spielfeld.length; i++) {
            for (int j = 0; j < spielfeld.length; j++) {
                if (spielfeld[i][j] < 10) {
                    System.out.print(spielfeld[i][j] + "  | ");
                } else {
                    System.out.print(spielfeld[i][j] + " | ");
                }

            }
            System.out.println();
        }

    }

    public static void spielen() {
        int z1 = 0;
        int z2 = 0;
        int z3 = 0;
        int z4 = 0;
        for (int i = 0; i < spielfeld.length; i++) {
            for (int j = 0; j < spielfeld.length; j++) {
                if (0 == spielfeld[i][j]) {
                    if (i - 1 >= 0) {
                        z1 = spielfeld[i - 1][j];
                    }
                    if (i + 1 <= 3) {
                        z2 = spielfeld[i + 1][j];
                    }
                    if (j - 1 >= 0) {
                        z3 = spielfeld[i][j - 1];
                    }
                    if (j + 1 <= 3) {
                        z4 = spielfeld[i][j + 1];
                    }

                }

            }
        }

        int Zahl = Integer.parseInt(JOptionPane.showInputDialog("Welche Zahl wollen Sie verschieben?"));
        boolean ko = false;
        while (ko == false) {
            if (Zahl > 15) {
                Zahl = Integer.parseInt(JOptionPane.showInputDialog("Die Zahl darf nur von 1-15 gehen"));
            } else {
                ko = true;
            }
        }

        boolean ok = false;
        while (ok == false) {
            if (Zahl == z1 || Zahl == z2 || Zahl == z3 || Zahl == z4) {
                ok = true;
            } else {
                Zahl = Integer.parseInt(JOptionPane.showInputDialog("Bitte wählen Sie eine Nachbarzahl von 0"));
            }
        }

        int zahl1 = -1;
        int zahl2 = -1;

        for (int i = 0; i < spielfeld.length; i++) {
            for (int j = 0; j < spielfeld.length; j++) {

                if (spielfeld[i][j] == Zahl) {
                    spielfeld[i][j] = 0;
                    zahl1 = i;
                    zahl2 = j;
                }
                if (spielfeld[i][j] == 0) {
                    spielfeld[i][j] = Zahl;
                    if (zahl1 == -1 && zahl2 == -1) {
                    } else {
                        spielfeld[zahl1][zahl2] = 0;
                    }
                }

            }
        }

    }

    public static void Überprüfen() {
        int k = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (spielfeld[i][j] == fertigesSpiel[i][j]) {
                    k++;
                } else {
                }
                if (k == 16) {

                    überprüfen = true;

                } else {
                    überprüfen = false;

                }
            }
        }

    }
}
